using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Pedido
    {
        public int PedidoId { get; set; }
        public int VendedorId { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusPedido Status { get; set; }  
    }
}