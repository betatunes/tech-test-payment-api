using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class PedidoVenda
    {
        public int PedidoVendaId { get; set; }
        public int VendaId { get; set; }
        public int ProdId { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
       
    }
}