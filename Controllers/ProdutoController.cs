using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly VendaContext _context;

        public ProdutoController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("CriarProduto")]
        public IActionResult CriarProduto(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();
            return CreatedAtAction("CriarProduto", new { id = produto.ProdutoId }, produto);
        }


        [HttpGet("ObterTodosOsProdutos")]
        public IActionResult ObterTodos()
        {
            return Ok(_context.Produtos);
        }

       
    }
}