using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;



namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

    [HttpPost("CriarPedido")]
    public IActionResult  CriarPedidoVenda(Pedido pedido)
    {    
       
       _context.Add(pedido);
       _context.SaveChanges();
       return CreatedAtAction("CriarPedidoVenda", new { id = pedido.PedidoId}, pedido);
       
    }
   
    [HttpPost("AdicionandoProdutosNoPedido")]
    public IActionResult  AdicionandoProdutosNoPedido(PedidoVenda pedidoVenda)
    {
      
       
        var produto = _context.Produtos.Find(pedidoVenda.ProdId);
        pedidoVenda.Valor = 0;
        if(produto== null)
            return BadRequest(new { Erro = "Para que o pedido seja efetivado com sucesso pelo menos 1 produto deve ser informado!!" });
        if(pedidoVenda.VendaId==0)
             return BadRequest(new { Erro = "Para que o produto escolhido seja adicionado no pedido o campo vendaId deve ser informado!!!!" });
        if(pedidoVenda.Quantidade==0)
            return BadRequest(new { Erro = "Favor informar a quantidade do produto desejada!!" });

        pedidoVenda.Valor = pedidoVenda.Valor + produto.Preco;
        _context.Add(pedidoVenda);
        _context.SaveChanges();
        return CreatedAtAction("AdicionandoProdutosNoPedido", new { id = pedidoVenda.PedidoVendaId }, pedidoVenda);
    }
    

    [HttpPut("AtualizarPedido")]
        
    public IActionResult Atualizar(int id, EnumStatusPedido status)
    {
          
        var pedidoBanco = _context.Pedidos.Find(id);
            

        if (pedidoBanco == null)
            return NotFound();

        
            if(pedidoBanco.Status==EnumStatusPedido.Entregue) 
                return BadRequest(new { Erro = "O status não pode ser atualizado, pois o pedido já conta como entregue" });
            else { 
             if(pedidoBanco.Status==EnumStatusPedido.EnviadoParaTransportadora && status==EnumStatusPedido.Cancelada)
                return BadRequest(new { Erro = "O status não pode ser atualizado, pois o mesmo não está seguindo a regra de atualização do Pedido" });  
            else          
            if(pedidoBanco.Status==EnumStatusPedido.AguardandoPagamento && status==EnumStatusPedido.PagamentoAprovado || status==EnumStatusPedido.Cancelada)
            pedidoBanco.Status = status;     
            else
                if(pedidoBanco.Status==EnumStatusPedido.PagamentoAprovado && status==EnumStatusPedido.EnviadoParaTransportadora || status==EnumStatusPedido.Cancelada)
                    pedidoBanco.Status = status;
            else
                if(pedidoBanco.Status==EnumStatusPedido.EnviadoParaTransportadora && status==EnumStatusPedido.Entregue)
                    pedidoBanco.Status = status;
           
            else
                return BadRequest(new { Erro = "O status não pode ser atualizado, pois o mesmo não está seguindo a regra de atualização do Pedido" }); 

        }                      
                                    
            _context.Pedidos.Update(pedidoBanco);
            _context.SaveChanges();
            
            return Ok(pedidoBanco);
            
    }
    [HttpGet("ObterVendaPor{id}")]
    public IActionResult ObterVendaPor(int id)
    {


        var vendas =
        from pv in _context.PedidosVenda
        join p in _context.Pedidos on pv.VendaId equals p.PedidoId	
        join ve in _context.Vendedores on p.VendedorId equals ve.Id	
        join prd in _context.Produtos on pv.ProdId equals prd.ProdutoId
        where pv.VendaId.Equals(id)
        orderby pv.VendaId
    
        select new
        {
            NumeroDaVenda = pv.VendaId,
            Produto = prd.NomeProduto,
            Preco = prd.Preco,
            Vendedor = ve.Nome,
            StatusVenda = p.Status
              
        };		

        return Ok(vendas);   

       
    }
  

    [HttpGet("ObterTodaAsVendas")]
    public async Task<IActionResult> ObterTodaAsVendas()
    {

        var vendas =
        from pv in _context.PedidosVenda
        join p in _context.Pedidos on pv.VendaId equals p.PedidoId	
        join ve in _context.Vendedores on p.VendedorId equals ve.Id	
        join prd in _context.Produtos on pv.ProdId equals prd.ProdutoId
        orderby pv.VendaId
        select new
        {
            NumeroDaVenda = pv.VendaId,
            Produto = prd.NomeProduto,
            Preco = prd.Preco,
            Vendedor = ve.Nome,
            StatusVenda = p.Status
        };		
        return Ok(vendas);               
    }

 }      
  
}
 
